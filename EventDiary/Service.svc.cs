﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EventDiary {
    public class Service : IService {

        System.Data.SqlClient.SqlConnection con;

        private void _CheckDbCon() {
            if (con == null) {
                con = new System.Data.SqlClient.SqlConnection();
                con.ConnectionString = @"Server=.\SQLEXPRESS;Database=EventDiary;Trusted_Connection=True;";
            }
        }

        private void _SetToken(string id, string token) {
            _CheckDbCon();
            SqlCommand cmd;
            if (_DoesTokenExists(id)) {
                cmd = new SqlCommand("UPDATE Tokens SET Token=@TOKEN, ExpiryDate=@EXPIRYDATE WHERE UserId = @USERID", con);
            } else {
                cmd = new SqlCommand("INSERT INTO Tokens VALUES (@TOKEN, @USERID, @EXPIRYDATE)", con);
            }
            cmd.Parameters.AddWithValue("@USERID", id);
            cmd.Parameters.AddWithValue("@TOKEN", token);
            cmd.Parameters.AddWithValue("@EXPIRYDATE", DateTime.Now.AddHours(12));
            try {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            } catch (Exception ex) {
                throw ex;
            }
        }

        private bool _DoesTokenExists(string id) {
            bool result;
            _CheckDbCon();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Tokens WHERE UserId = @USERID", con);
            cmd.Parameters.AddWithValue("@USERID", id);
            try {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    result = reader.Read();
                    // Doesn't matter if token is not longer valid, it will be updated in SetToken anyway
                }
                con.Close();
            } catch (Exception ex) {
                throw ex;
            }
            return result;
        }

        private int _CheckTokenValidity(string token) {
            int result;
            _CheckDbCon();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Tokens WHERE Token = @TOKEN", con);
            cmd.Parameters.AddWithValue("@TOKEN", token);
            try {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        result = Convert.ToInt32(reader["UserId"].ToString());
                        if (DateTime.Parse(reader["ExpiryDate"].ToString()) < DateTime.Now) {
                            // DELETE TOKEN
                            result = -1;
                            // Because token is not valid anymore, the user wil have to reauthenticate and the token will be refreshed
                        }
                    } else {
                        result = -1;
                    }
                }
                con.Close();
            } catch (Exception ex) {
                throw ex;
            }
            return result;
        }

        private void _DiscardOldLogs(int logbook_id) {
            _CheckDbCon();
            SqlCommand cmd = new SqlCommand("DELETE FROM Logs WHERE DateTime < @NOW AND LogBookId = @ID", con);
            cmd.Parameters.AddWithValue("@ID", logbook_id);
            cmd.Parameters.AddWithValue("@NOW", DateTime.Now.AddHours(-48));
            try {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            } catch (Exception ex) {
                throw ex;
            }
        }

        public int addLog(Log log, string token) {
            _CheckDbCon();
            int user_id = _CheckTokenValidity(token);
            if (user_id == -1) {
                return -1;
            }
            _DiscardOldLogs(log.LogBookId);
            int i;
            SqlCommand cmd = new SqlCommand("INSERT into Logs (DateTime, Message, LogBookId) VALUES (@DATETIME, @MESSAGE, @LOGBOOKID)", con);
            cmd.Parameters.AddWithValue("@DATETIME", log.Time);
            cmd.Parameters.AddWithValue("@MESSAGE", log.Message);
            cmd.Parameters.AddWithValue("@LOGBOOKID", log.LogBookId);
            try {
                con.Open();
                i = cmd.ExecuteNonQuery();
                con.Close();
            } catch {
                return -1;
            }
            return i;
        }

        public List<Log> getLogsFromInterval(int logbook_id, DateTime from, DateTime to, string token) {
            List<Log> result = new List<Log>();
            if (_CheckTokenValidity(token) == -1) {
                return result;
            }
            if (from < DateTime.Now.AddHours(-48)) from = DateTime.Now.AddHours(-48);
            _CheckDbCon();
            SqlCommand cmd = new SqlCommand("SELECT * from Logs WHERE DateTime BETWEEN @FROM AND @TO AND LogBookId = @ID", con);
            cmd.Parameters.AddWithValue("@FROM", from);
            cmd.Parameters.AddWithValue("@TO", to);
            cmd.Parameters.AddWithValue("@ID", logbook_id);
            try {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    while (reader.Read()) {
                        Log log = new Log();
                        log.Time = (DateTime)reader["DateTime"];
                        log.Message = reader["Message"].ToString();
                        log.LogBookId = logbook_id;
                        result.Add(log);
                    }
                }
                con.Close();
            } catch (Exception e) {
                throw e;
            }
            return result;
        }

        public string Authenticate(string username, string password) {
            string token = null, id = null;
            _CheckDbCon();
            SqlCommand cmd = new SqlCommand("SELECT * from Costumers WHERE Username = @USERNAME", con);
            cmd.Parameters.AddWithValue("@USERNAME", username);
            try {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        if (BCrypt.CheckPassword(password, reader["Password"].ToString())) {
                            // Login Succesfull
                            token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                            id = reader["Id"].ToString();
                        } else {
                            token = null;
                        }
                    }
                }
                con.Close();
            } catch (Exception ex) {
                throw ex;
            }
            if (token != null) {
                _SetToken(id, token);
            }
            return token;
        }

        public string GetPassword(string pass) {
            return BCrypt.HashPassword(pass, BCrypt.GenerateSalt(12));
        }
    }
}
