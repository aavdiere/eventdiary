﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EventDiary {
    /*
     * Remarks on design strategie:
     *** Password hashing
     *** Search time (one-to-many relationship)
     *** Removing all old logs or only the one from current logbook
     *** 'Check if token is valid' does not require UserId, this could be harmfull but if the user also has to provide the UserId it is no different from providing the username and password each time. For security it's also not that important because if a hacker aquires the hash it's a small effort to loop through all userid's
     */
    [ServiceContract]
    public interface IService {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "login/", ResponseFormat = WebMessageFormat.Json)]
        string Authenticate(string username, string password);

        // This method is for testing
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "getpassword/{pass}", ResponseFormat = WebMessageFormat.Json)]
        string GetPassword(string pass);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "add/", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        int addLog(Log log, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "get/{logbook_id}/{from}/{to}", ResponseFormat = WebMessageFormat.Json)]
        List<Log> getLogsFromInterval(int logbook_id, DateTime from, DateTime to, string token);
    }
}
