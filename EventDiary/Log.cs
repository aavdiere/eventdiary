﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventDiary {
    public class Log : IComparable<Log> {
        public DateTime Time {
            get;
            set;
        }

        public string Message {
            get;
            set;
        }

        public int LogBookId {
            get;
            set;
        }

        public int CompareTo(Log other) {
            if (Time < other.Time) return -1;
            else if (Time > other.Time) return 1;
            else return 0;
        }
    }
}